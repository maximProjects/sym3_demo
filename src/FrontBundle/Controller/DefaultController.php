<?php

namespace FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            die('login out');
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();

        return $this->render('FrontBundle:Default:index.html.twig');
    }
}
