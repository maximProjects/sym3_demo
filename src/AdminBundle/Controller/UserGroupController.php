<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\UserGroup;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Usergroup controller.
 *
 */
class UserGroupController extends Controller
{
    /**
     * Lists all userGroup entities.
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $userGroups = $em->getRepository('AdminBundle:UserGroup')->findAll();
        return [
            'userGroups' => $userGroups
        ];
    }

    /**
     * Creates a new userGroup entity.
     * @Template()
     */
    public function newAction(Request $request)
    {
        $userGroup = new Usergroup();
        $form = $this->createForm('AdminBundle\Form\UserGroupType', $userGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($userGroup);
            $em->flush($userGroup);
            $this->addFlash(
                'success',
                'Group created!'
            );
            return $this->redirectToRoute('usergroup_index', array('id' => $userGroup->getId()));
        }

        return [
            'userGroup' => $userGroup,
            'form' => $form->createView()
        ];
    }

    /**
     * Displays a form to edit an existing userGroup entity.
     * @Template()
     */
    public function editAction(Request $request, UserGroup $userGroup)
    {
        $deleteForm = $this->createDeleteForm($userGroup);
        $editForm = $this->createForm('AdminBundle\Form\UserGroupType', $userGroup);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'success',
                'Group updated!'
            );
            return $this->redirectToRoute('usergroup_index', array('id' => $userGroup->getId()));
        }

        return [
            'userGroup' => $userGroup,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a userGroup entity.
     *
     */
    public function deleteAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $group = $em->getRepository('AdminBundle:UserGroup')->findOneById($id);
        if(count($group->getUsers())==0) {
            $em->remove($group);
            $em->flush();
            $this->addFlash(
                'danger',
                'Group deleted!'
            );
        } else {
            $this->addFlash(
                'warning',
                'Can not delete. This group have users!'
            );
        }
        return $this->redirectToRoute('usergroup_index');
    }

    /**
     * Creates a form to delete a userGroup entity.
     *
     * @param UserGroup $userGroup The userGroup entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(UserGroup $userGroup)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usergroup_delete', array('id' => $userGroup->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
