<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\UserEditType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AdminBundle\Form\UserType;
use AdminBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Template()
     */
    public function indexAction()
    {
        $users = $this->getDoctrine()->getRepository('AdminBundle:User')->findAll();
//        return $this->render('AdminBundle:Default:index.html.twig');
        return ['users' => $users];
    }

    /**
     * @Template()
     */
    public function editAction(Request $request, $id = null)
    {
        $user = $this->getDoctrine()->getRepository('AdminBundle:User')->findOneById($id);
        $form = $this->createForm(UserEditType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $this->getDoctrine()->getManager()->flush();
                $this->addFlash(
                    'success',
                    'User updated!'
                );
                return $this->redirectToRoute('admin_homepage');
            }
        }
        return ['form' => $form->createView()];
    }

    public function deleteAction(Request $request, $id = null)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('AdminBundle:User')->findOneById($id);

        //if(count($user->getUserGroups())==0) {
            $em->remove($user);
            $em->flush();
            $this->addFlash(
                'danger',
                'User deleted'
            );
       // }
        return $this->redirectToRoute('admin_homepage');
    }

    /**
     * @Template()
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user
            $this->addFlash(
                'success',
                'User created!'
            );
            return $this->redirectToRoute('admin_homepage');
        }
        return ['form' => $form->createView()];
    }
}